package com.example.mybookshopapp.selenium;

import org.openqa.selenium.chrome.ChromeDriver;

public class GenresPage extends AbstractPage {

    public GenresPage(ChromeDriver driver) {
        super(driver);
    }

    @Override
    protected AbstractPage callPage() {
        driver.get(super.url + "genres");
        return this;
    }
}
