package com.example.mybookshopapp.data.genre;

import com.example.mybookshopapp.data.book.Book;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "genres")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "INT")
    private Integer parentId;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String slug;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String name;

    @OneToMany(mappedBy = "genre")
    private List<Book> bookListByGenre = new ArrayList<>();

    public List<Book> getBookListByGenre() {
        return bookListByGenre;
    }

    public void setBookListByGenre(List<Book> bookListByGenre) {
        this.bookListByGenre = bookListByGenre;
    }

    @Override
    public String toString() {
        return name;
    }
}
