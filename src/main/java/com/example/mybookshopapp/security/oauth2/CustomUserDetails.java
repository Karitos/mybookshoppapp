package com.example.mybookshopapp.security.oauth2;

import org.springframework.security.core.userdetails.UserDetails;

public interface CustomUserDetails extends UserDetails {

    String getEmail();
}

