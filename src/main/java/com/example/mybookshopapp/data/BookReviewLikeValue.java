package com.example.mybookshopapp.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookReviewLikeValue {

    private Integer value;
    private Integer reviewId;

}
