package com.example.mybookshopapp.security;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "users")
public class BookStoreUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String login;
    private String email;
    private String phone;
    private String password;

    @Column(name = "is_oauth2")
    @ColumnDefault("false")
    private Boolean isOAuth2;

    @Column(name = "id_oauth")
    private Integer idOAuth;

    public Boolean getOAuth2() {
        return isOAuth2;
    }

    public void setOAuth2(Boolean OAuth2) {
        isOAuth2 = OAuth2;
    }

    public Integer getIdOAuth() {
        return idOAuth;
    }

    public void setIdOAuth(Integer idOAuth) {
        this.idOAuth = idOAuth;
    }

}
