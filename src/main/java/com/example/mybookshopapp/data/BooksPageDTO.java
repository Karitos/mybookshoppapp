package com.example.mybookshopapp.data;

import com.example.mybookshopapp.data.book.Book;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BooksPageDTO {
    private Integer count;
    private List<Book> books;

    public BooksPageDTO(List<Book> books) {
        this.books = books;
        this.count = books.size();

    }

}
