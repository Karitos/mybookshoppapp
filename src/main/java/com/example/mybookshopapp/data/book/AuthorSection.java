package com.example.mybookshopapp.data.book;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class AuthorSection {

    String letter;
    List<Author> authors;


    @Override
    public String toString() {
        return "AuthorSection{" +
                "letter='" + letter + '\'' +
                ", authors=" + authors +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorSection that = (AuthorSection) o;
        return Objects.equals(letter, that.letter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(letter);
    }
}
