package com.example.mybookshopapp.data.book.rating;

import com.example.mybookshopapp.data.book.Book;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.Objects;

@Entity
@Table(name = "book_rating")
@Getter
@Setter
@Transactional
public class BookRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    @JsonIgnore
    private Book book;

    @Column(name = "one_star")
    private Integer oneStar;

    @Column(name = "two_stars")
    private Integer twoStars;

    @Column(name = "three_stars")
    private Integer threeStars;

    @Column(name = "four_stars")
    private Integer fourStars;

    @Column(name = "five_stars")
    private Integer fiveStars;

    public void setStarByValue(Integer starValue) {
        switch (starValue) {
            case 1:
                this.setOneStar(getOneStar() + 1);
                break;
            case 2:
                this.setTwoStars(getTwoStars() + 1);
                break;
            case 3:
                this.setThreeStars(getThreeStars() + 1);
                break;
            case 4:
                this.setFourStars(getFourStars() + 1);
                break;
            case 5:
                this.setFiveStars(getFiveStars() + 1);
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookRating that = (BookRating) o;
        return Objects.equals(id, that.id) && Objects.equals(book, that.book) && Objects.equals(oneStar, that.oneStar) && Objects.equals(twoStars, that.twoStars) && Objects.equals(threeStars, that.threeStars) && Objects.equals(fourStars, that.fourStars) && Objects.equals(fiveStars, that.fiveStars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, book, oneStar, twoStars, threeStars, fourStars, fiveStars);
    }

    @Override
    public String toString() {
        return "BookRating{" +
                "id=" + id +
                ", book=" + book +
                ", oneStar=" + oneStar +
                ", twoStars=" + twoStars +
                ", threeStars=" + threeStars +
                ", fourStars=" + fourStars +
                ", fiveStars=" + fiveStars +
                '}';
    }

    public Integer getAverageRating() {
        if (this.oneStar != 0 && this.twoStars != 0 && this.threeStars != 0 && this.fourStars != 0 && this.fiveStars != 0) {
            return Math.toIntExact((this.oneStar + this.twoStars * 2 + this.threeStars * 3L + this.fourStars * 4 + this.fiveStars * 5L)
                    / (this.oneStar + this.twoStars + this.threeStars + this.fourStars + this.fiveStars));
        } else {
            return 0;
        }
    }

    public Integer getAllVoteAmount() {
        return this.oneStar + this.twoStars + this.threeStars + this.fourStars + this.fiveStars;
    }
}
